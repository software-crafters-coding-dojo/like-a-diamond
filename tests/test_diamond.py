import pytest

from diamond import __version__
from diamond.diamond import line_count, diamonize, space_count, mine_raw_layer, raffine_layer, letter_order


def test_version():
    assert __version__ == '0.1.0'


def test_given_A_return_just_A():
    # Arrange
    expected = "A"
    # Act
    result = diamonize("A")
    # Assert
    assert result == expected


def test_given_B_return_B_diamond():
    # Arrange
    expected = " A \nB B\n A "
    # Act
    result = diamonize("B")
    # Assert
    assert result == expected


def test_given_C_return_C_diamond():
    # Arrange
    expected = "  A  \n B B \nC   C\n B B \n  A  "
    # Act
    result = diamonize("C")
    # Assert
    assert result == expected


@pytest.mark.parametrize("expected,letter", [
    (1, "A"),
    (3, "B"),
    (5, "C"),
])
def test_number_line_for_letter_return_expected(expected, letter):
    # Arrange
    # Act
    result = line_count(letter)
    # Assert
    assert result == expected


@pytest.mark.parametrize("expected,letter", [
    ("CBABC", "C"),
    ("BAB", "B")
])
def test_raw_lawer(expected, letter):
    # Arrange
    # Act
    result = mine_raw_layer(letter)
    # Assert
    assert result == expected





@pytest.mark.parametrize("expected,layer,letter", [
    ("C   C", "CBABC", "C"),
    ("B B", "BAB", "B")
])
def test_raffine_layer(expected, layer, letter):
    # Arrange
    # Act
    result = raffine_layer(layer, letter)
    # Assert
    assert result == expected

def test_letter_position():
    assert 3 == ord('D') - ord('A')

@pytest.mark.parametrize("expected,letter", [
    ("ABCBA", "C"),
    ("ABA", "B")
])
def test_letter_order(expected, letter):
    # Arrange

    # Act
    result =  letter_order(letter)
    # Assert
    assert result == expected

"  A  " \
" B B " \
"C   C" \
" B B " \
"  A  "

"   A   " \
"  B B  " \
" C   C " \
"D     D"
" C   C " \
"  B B  " \
"   A   "
