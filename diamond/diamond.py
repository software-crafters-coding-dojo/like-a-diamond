def line_count(letter: str) -> int:
    return (ord(letter) - ord('A') + 1) * 2 - 1

def letter_order(letter: str) ->str:
    current_letter: int = ord('A')
    result: str = ""
    while current_letter < ord(letter):
        result += chr(current_letter)
        current_letter += 1
    while current_letter >= ord('A'):
        result += chr(current_letter)
        current_letter -= 1
    return result


def diamonize(letter: str) -> str:
    diamond = []
    raw_line = mine_raw_layer(letter)
    for line_letter in letter_order(letter) :
        diamond.append(raffine_layer(raw_line, line_letter))
    return "\n".join(diamond)



    if letter == 'B':
        return " A \nB B\n A "
    if letter == 'C':
        return "  A  \n B B \nC   C\n B B \n  A  "
    return letter


def space_count(letter_origin, letter_target) -> int:
    return 2

def mine_raw_layer(param):
    curent_letter = param
    result = ""
    while ord(curent_letter) >  ord('A'):
        result += curent_letter
        curent_letter= chr(ord(curent_letter)-1)
    result += 'A' + result[len(result)::-1]
    return result

def raffine_layer(layer : str, letter ):
    result =[]
    for current_letter in layer :
        if current_letter == letter:
            result.append(current_letter)
        else:
            result.append(" ")
    return "".join(result)